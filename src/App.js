import React, { useState } from 'react';
import Header from './components/Header.jsx';
import Page from './components/Page.jsx';
import Footer from './components/Footer';
import './styles/App.scss'

function App() {


  return (
    <div className='wrapper'>
      <Header />
      <Page />
      <Footer />
    </div>

  )
}


export default App;