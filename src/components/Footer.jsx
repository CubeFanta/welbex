import React from 'react';
import logoTelegram from '../logo/Telegram.png'
import logoViber from '../logo/Viber.png'
import logoWatsApp from '../logo/WatsApp.png'
import '../styles/App.scss'

const Footer = () => {

	return (
		<div className='footer'>
			<div className='footer__info-block info-block _container'>
				<div className='info-block__list'>
					<div className='footer__company company'>
						<ul className='company__list'>
							<li className='company__item-title'>
								<h1 className='footer__title'>О компании</h1>
							</li>
							<li className='company__item'>
								<a href='/' className='footer__link'>Партнёрская программа</a>
							</li>
							<li className='company__item'>
								<a href='/' className='footer__link'>Вакансии</a>
							</li>
						</ul>
					</div>
					<div className='footer__menu1 menu1'>
						<ul className='menu1__list'>
							<li className='menu1__item-title'>
								<h1 className='footer__title'>Меню</h1>
							</li>
							<li className='menu1__item'>
								<a href='/' className='footer__link'>Расчёт стоимости</a>
							</li>
							<li className='menu1__item'>
								<a href='/' className='footer__link'>Услуги</a>
							</li>
							<li className='menu1__item'>
								<a href='/' className='footer__link'>Виджеты</a>
							</li>
							<li className='menu1__item'>
								<a href='/' className='footer__link'>Интеграции</a>
							</li>
							<li className='menu1__item'>
								<a href='/' className='footer__link'>Наши клиенты</a>
							</li>
						</ul>
					</div>
					<div className='footer__menu2 menu2'>
						<ul className='menu2__list'>
							<li className='menu2__item'>
								<a href='/' className='footer__link'>Кейсы</a>
							</li>
							<li className='menu2__item'>
								<a href='/' className='footer__link'>Благодарственные письма</a>
							</li>
							<li className='menu2__item'>
								<a href='/' className='footer__link'>Сертификаты</a>
							</li>
							<li className='menu2__item'>
								<a href='/' className='footer__link'>Блог на Youtube</a>
							</li>
							<li className='menu2__item'>
								<a href='/' className='footer__link'>Вопрос / Ответ</a>
							</li>
						</ul>
					</div>
					<div className='footer__mobile mobile'>
						<div className='footer__mobile-menu1 mobile-menu1'>
							<ul className='menu1__list'>
								<li className='menu1__item-title'>
									<h1 className='footer__title'>Меню</h1>
								</li>
								<li className='menu1__item'>
									<a href='/' className='footer__link'>Расчёт стоимости</a>
								</li>
								<li className='menu1__item'>
									<a href='/' className='footer__link'>Услуги</a>
								</li>
								<li className='menu1__item'>
									<a href='/' className='footer__link'>Виджеты</a>
								</li>
								<li className='menu1__item'>
									<a href='/' className='footer__link'>Интеграции</a>
								</li>
								<li className='menu1__item'>
									<a href='/' className='footer__link'>Наши клиенты</a>
								</li>
							</ul>
						</div>
						<div className='footer__mobile-menu2 mobile-menu2'>
							<ul className='menu2__list'>
								<li className='menu2__item'>
									<a href='/' className='footer__link'>Благодарность клиента</a>
								</li>
								<li className='menu2__item'>
									<a href='/' className='footer__link'>Кейсы</a>
								</li>
								<li className='menu2__item'>
									<a href='/' className='footer__link'>Сертификаты</a>
								</li>
								<li className='menu2__item'>
									<a href='/' className='footer__link'>Блог на Youtube</a>
								</li>
								<li className='menu2__item'>
									<a href='/' className='footer__link'>Вопрос / Ответ</a>
								</li>
							</ul>
						</div>
					</div>
					<div className='footer__contacts contacts'>
						<ul className='contacts__list'>
							<li className='contacts__item-title'>
								<h1 className='footer__title'>Контакты</h1>
							</li>
							<li className='contacts__item'>
								<a href='/' className='footer__link'>+7 555 555-55-55</a>
							</li>
							<li className='contacts__item'>
								<img src={logoTelegram} className="contacts__logo-teleram" alt='' />
								<img src={logoViber} className="contacts__logo-viber" alt='' />
								<img src={logoWatsApp} className="contacts__logo-watsapp" alt='' />
							</li>
							<li className='contacts__item'> <a href='/' className='footer__link'>Москва, Путевой проезд 3с1, к 902</a>
							</li>
						</ul>
					</div>

				</div>

			</div>
			<div className='footer__running-title running-title _container'>
				<p className='running__text'>©WELBEX 2022. Все права защищены.</p>
				<a href='/' className='running__link'>Политика конфиденциальности</a>
			</div>
		</div>
	)
}

export default Footer;