import React from 'react';
import '../styles/App.scss'

const Page = () => {

	return (
		<div className='page'>
			<div className='page__main-block main-block _container'>
				<div className='main-block__body body-main'>
					<div className='page__column-left left-column'>
						<h1 className='left-column__app'>Зарабатывайте <br></br> больше</h1>
						<h1 className='left-column__down'>с WELBEX</h1>
						<div className='left-column__texts'>Развиваем и контролируем<br></br> продажи за вас</div>
					</div>
					<div className='page-column__mobile mobile-page'>
						<div className='mobile-page__header-mobile header-mobile'>
							<h1 className='header-mobile__title'>Вместе с <span>бесплатной<br></br> консультацией</span> мы дарим:</h1>
						</div>
						<div className='mobile-page__block-top'>
							<span className='mobile-page__line'></span><h1 className='mobile-page__title'>Skype аудит</h1>
							<span className='mobile-page__line'></span><h1 className='mobile-page__title'>30 виджетов</h1>
						</div>
						<div className='mobile-page__block-down'>
							<span className='mobile-page__line'></span><h1 className='mobile-page__title'>Dashboard</h1>
							<span className='mobile-page__line'></span><h1 className='mobile-page__title'>Месяц аmoCRM</h1>
						</div>
					</div>
					<div className='page__column-right right-column'>
						<div className='right-column__header-offer header-offer'>
							<h1 className='header-offer__title'>Вместе с<span>бесплатной<br></br> консультацией</span> мы дарим:</h1>
						</div>
						<div className='right-column__list list'>
							<div>
								<div className='list__offer'>
									<h1 className='list__title'>Виджеты</h1>
									<a href='/' className='list__link'>30 готовых<br></br> решений</a>
								</div>
								<div className='list__offer'>
									<h1 className='list__title'>Skype Аудит</h1>
									<a href='/' className='list__link'>отдела продажи <br></br> и CRM системы</a>
								</div>
							</div>
							<div>
								<div className='list__offer'>
									<h1 className='list__title'>Dashboard</h1>
									<a href='/' className='list__link'>с показателями <br></br> вашего бизнеса</a>
								</div>
								<div className='list__offer'>
									<h1 className='list__title'>35 дней</h1>
									<a href='/' className='list__link'>использования <br></br> CRM</a>
								</div>
							</div>
						</div>
						<div className='right-column__button button'>
							<a href='/' className='button__link'>Получить консультацию</a>
						</div>
					</div>
				</div>
			</div>
			<div className='page__big-ball_red'></div>
			<div className='page__medium-ball_purple'></div>
			<div className='page__little-ball_red'></div>
			<div className='page__light-ball_red'></div>
			<div className='page__light-ball_purple'></div>
		</div>
	)
}

export default Page;