import React from 'react';
import logoCompanyTwo from '../logo/Vector2.png'
import logoCompanyOne from '../logo/Vector1.png'
import logoTelegram from '../logo/Telegram.png'
import logoViber from '../logo/Viber.png'
import logoWatsApp from '../logo/WatsApp.png'
import '../styles/App.scss'

const Header = () => {

	return (
		<div className='header'>
			<div className='header__container _container'>
				<div className='heder__logo logo'>
					<img src={logoCompanyOne} className="logo__item-one" alt='' />
					<img src={logoCompanyTwo} className="logo__item-two" alt='' />
					<p className='logo__info'>крупнейший интегратор CRM<br></br> в России и еще в 8 странах</p>
				</div>
				<nav className='header__menu menu'>
					<ul className='menu__list'>
						<li className='menu__item'>
							<a href='/' className='menu__link'>Услуги</a>
						</li>
						<li className='menu__item'>
							<a href='/' className='menu__link'>Виджеты</a>
						</li>
						<li className='menu__item'>
							<a href='/' className='menu__link'>Интеграции</a>
						</li>
						<li className='menu__item'>
							<a href='/' className='menu__link'>Кейсы</a>
						</li>
						<li className='menu__item'>
							<a href='/' className='menu__link'>Сертификаты</a>
						</li>
					</ul>
				</nav>
				<div className='header__contacts contacts'>
					<span className='contacts__phone'>+7 555 555-55-55</span>
					<img src={logoTelegram} className="contacts__logo-telegram" alt='' />
					<img src={logoViber} className="contacts__logo-viber" alt='' />
					<img src={logoWatsApp} className="contacts__logo-watsapp" alt='' />
				</div>
			</div>
		</div>
	)
}

export default Header;